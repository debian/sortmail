/*	@(#)locking.h 1.2 02/03/11 falk	*/
/*	$Id: locking.h,v 1.1.1.1 2002/03/12 05:38:52 efalk Exp $	*/

typedef	struct {
	  char	*dotname ;
	  int	fd ;
	} LockInfo ;

#ifdef	__STDC__
extern	int	lockFile(char *, int flags, mode_t, bool wait, LockInfo *lock);
extern	void	unLockFile(LockInfo *lock) ;
#else
extern	int	lockFile() ;
extern	void	unLockFile() ;
#endif
