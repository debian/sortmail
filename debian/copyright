Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sortmail
Source: http://sortmail.sourceforge.net
Comment: sortmail originally comes from comp.sources.unix volume 28.

Files: *
Copyright: 1990-2003 Edward A. Falk <falk@efalk.org>
License: MIT

Files: crctab.c
Copyright: 1986 Gary S. Brown
License: Special

Files: md5.h
       md5c.c
Copyright: 1991-1992 RSA Data Security, Inc.
License: RSA

Files: regex.h
Copyright: 1985 Free Software Foundation, Inc.
License: GPL-1+

Files: debian/*
Copyright: 1997-2004 Hamish Moffatt <hamish@debian.org>
           2016      Sean Whitton <spwhitton@spwhitton.name>
           2018      Gianfranco Costamagna <locutusofborg@debian.org>
           2021      Joao Eriberto Mota Filho <eriberto@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: Special
 You may use this program, or code or tables extracted from it,
 as desired without restriction.

License: RSA
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

License: GPL-1+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This pprogram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 1 can be found in "/usr/share/common-licenses/GPL-1".
