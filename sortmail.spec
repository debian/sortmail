# $Id: sortmail.spec,v 1.11 2003/12/22 06:03:14 efalk Exp $

%define	ver	2.4
%define	RELEASE	1
%define	prefix	/usr

Summary: sortmail - download and classify email
Name: sortmail
Version: %ver
Release: %RELEASE
Group: Applications/Communications
URL: http://sortmail.sourceforge.net
Copyright: MIT
Packager: Ed Falk <falk@efalk.org>
Source: http://sortmail.sourceforge.net/sortmail-%{PACKAGE_VERSION}.tar.gz
Buildroot: %{_tmppath}/%{name}-root
Docdir: %prefix/doc
Prefix: %prefix

%description
Sortmail is a program to process incoming email, classify it, and process
it accordingly.  Sortmail can handle incoming email as it arrives,
or download from a POP server.  Sortmail is easier to configure than
procmail, and can replace fetchmail.  Sortmail uses config files
which are similar to rn KILLfiles.

%prep
%setup
make clean

%build
make prefix=%prefix OPT="-O" LDOPTS="-s"

%install
rm -rf $RPM_BUILD_ROOT
make	prefix=$RPM_BUILD_ROOT%{prefix} \
	INSTALL="install -m0755" INSTALL_DAT="install -m0444" \
	install

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc LICENSE README sample.forward sample.sortmailrc sortmail.lsm
%doc sortmailrc.pop sortmailrc.sample sortmailrc.spam
%{prefix}/bin/sortmail
%{prefix}/man/man1/sortmail.1.gz
