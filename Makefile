# @(#)Makefile 1.12 02/03/11 falk
# $Id: Makefile,v 1.9 2003/12/22 06:03:14 efalk Exp $

# define NOSTRDUP if your system doesn't have strdup(3)
# define NOREGCOMP if your system doesn't have regcomp(3)
#
# Note: GNU regex is provided for systems that don't have the regex(3)
# interface.  If you don't have regex(3), set REGEX = regex.c below

#DBG = -g -DDEBUG -Wall
DBG = -O

# SunOS 4.x
#CFLAGS = ${DBG} -DBSD -DSUNOS
#LDFLAGS = -n -Bdynamic -s

# Solaris
# CFLAGS = ${DBG} -DSOLARIS
# LDFLAGS = -lxnet -lresolv

# Linux
CFLAGS = ${DBG} -DLINUX -I/usr/include/gdbm
LDFLAGS = -lgdbm

# Ultrix
#CFLAGS = ${DBG} -DULTRIX
#LDFLAGS =
#REGEX = regex.c

# HPUX 9.0
#CFLAGS = ${DBG} -DHPUX
#LDFLAGS = -n -s
#REGEX = regex.c


# ---------------------------------------------------------------------------
# Destination for make install:
#
# Note: version number in Makefile, INSTALL, sortmail.c, sortmail.lsm,
# sortmail.spec

VERSION = 2.4
prefix = /usr/local
bindir = $(prefix)/bin
docdir = $(prefix)/doc/sortmail-$(VERSION)
mandir = $(prefix)/man
man1dir = $(prefix)/man/man1

#CC = gcc

all: sortmail

SRCS = sortmail.c parse.c expr.c crctab.c pop2.c pop3.c imap.c \
	md5c.c netutils.c utils.c locking.c $(REGEX)

HDRS = config.h crctab.h imap.h locking.h md5.h md5global.h netutils.h \
	pop2.h pop3.h regex.h sortmail.h utils.h

OBJS = $(SRCS:.c=.o)

DOC = README sortmail.1 sample.forward sample.sortmailrc

$(OBJS): $(HDRS)

sortmail: $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJS) $(LIB)

tags: $(HDRS) $(SRCS)
	ctags *.[ch]

archive: Makefile $(HDRS) $(SRCS) $(DOC)
	shar $(DOC) Makefile $(HDRS) $(SRCS) > archive

clean:
	rm -f *.o *.i archive foo bar core tags

clobber: clean
	rm -f sortmail


sortmail.o:	sortmail.h config.h  pop2.h pop3.h locking.h utils.h crctab.h
parse.o:	sortmail.h config.h  utils.h
expr.o:		sortmail.h config.h  utils.h
pop2.o:		sortmail.h config.h  netutils.h pop2.h utils.h
pop3.o:		sortmail.h config.h  netutils.h pop3.h utils.h md5global.h md5.h
netutils.o:	sortmail.h config.h  netutils.h
utils.o:	sortmail.h config.h utils.h
locking.o:	sortmail.h config.h locking.h
md5c.o:		md5global.h md5.h


DESTDIRS = $(bindir) $(man1dir) $(docdir)

DOCFILES = LICENSE README sample.forward sample.sortmailrc sortmail.lsm \
	sortmailrc.pop sortmailrc.sample sortmailrc.spam


# Install sortmail

install: sortmail $(DESTDIRS)
	install -s sortmail $(bindir)
	install -m 0444 sortmail.1 $(man1dir)
	install -m 0444 $(DOCFILES) $(docdir)

$(DESTDIRS):
	mkdir -p $@




# Build distribution tarfile.

DIST_NAME = sortmail-$(VERSION)
DIST_DIR = /tmp/$(DIST_NAME)
DIST_FILES = $(SRCS) $(HDRS) Makefile sortmail.lsm sortmail.1 \
	$(DOCFILES) sortmail.spec

dist:	clobber $(DIST_FILES)
	rm -rf $(DIST_DIR)
	mkdir $(DIST_DIR)
	cp -pr $(DIST_FILES) $(DIST_DIR)
	chmod -R a+r $(DIST_DIR)
	chmod -R u+w $(DIST_DIR)
	chmod -R go-w $(DIST_DIR)
	cd $(DIST_DIR)/.. ; tar cvf $(DIST_NAME).tar $(DIST_NAME)
	mv $(DIST_DIR).tar .
	gzip $(DIST_NAME).tar
	rm -rf $(DIST_DIR)

depend:
	makedepend -- $(CFLAGS) -- $(SRCS)

