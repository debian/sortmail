/*	@(#)pop2.h 1.2 02/03/11 falk	*/
/*	$Id: pop2.h,v 1.1.1.1 2002/03/12 05:38:53 efalk Exp $	*/


	/* info known about a pop2 folder. */

typedef	struct {
	  int fd ;
	  int nmsg ;
	  int timeout ;
	} Pop2Folder ;





#ifdef	__STDC__
extern	Pop2Folder *openPop2( char *hostname, char *user, char *pw, int to) ;
extern	int	pop2Folder( Pop2Folder *p2, char *folder, int to) ;
extern	int	pop2GetMessage( MailMessage *, MsgState);
extern	int	pop2FinishMessage( Pop2Folder *p2, int save, int timeout) ;
extern	void	closePop2( Pop2Folder *p2) ;
#else
extern	Pop2Folder *openPop2() ;
extern	int	pop2Folder() ;
extern	int	pop2GetMessage();
extern	int	pop2FinishMessage() ;
extern	void	closePop2() ;
#endif
