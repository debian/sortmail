#ifndef lint
static const char sccsid[] = "@(#)imap.c 1.2 02/03/11 falk" ;
static const char rcsid[] = "$Id: imap.c,v 1.3 2003/08/21 07:07:34 efalk Exp $" ;
#endif

/*	IMAP4.C	-- manage IMAP4 protocol
 *
 *
 * ImapFolder *
 * openImap(char *hostname, char *user, char *pw, int to)
 *	Open a network connection.  Return NULL on failure.
 *
 *
 * int
 * imapFolder(ImapFolder *, char *folder, int to)
 *	Set input folder.  Return 0 on success, error code on
 *	failure.
 *
 * int
 * imapGetHeader(ImapFolder *, MailMessage *msg, int msgno, int to)
 *	Read one header from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 *
 * int
 * imapGetMessage(MailMessage *msg, MsgState)
 *	Read one message from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 *
 * int
 * imapDeleteMessage(ImapFolder *, int save)
 *	Acknowledge receipt of a message, tell server to save or delete.
 *
 * void
 * closeImap(ImapFolder *)
 *	Close connection.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <netinet/in.h>

#include "sortmail.h"
#include "netutils.h"
#include "utils.h"
#include "imap.h"
#include "md5global.h"
#include "md5.h"


#define	MAX_IMAP_CMD	512

typedef	enum {CONT, OK, NO, BAD, UNTAGGED, DISCONNECT} parseVal ;

static	ImapFolder	*ImapAbort(ImapFolder *im ) ;
static	parseVal	ImapResponse(ImapFolder *, MailMessage *, char *, int);
static	parseVal	ImapParse(ImapFolder *, MailMessage *, char *buffer) ;
static	parseVal	ParseData(ImapFolder *, MailMessage *, char *buffer) ;
static	parseVal	ParseCode(ImapFolder *, MailMessage *, char *buffer) ;
static	parseVal	ImapFetch(ImapFolder *, MailMessage *, char *buffer) ;
static	int	imapGetData( ImapFolder *, MailMessage *, int msgno, int to) ;
static	int	AuthKerberos(ImapFolder *, char *host) ;
static	int	AuthOtp(ImapFolder *) ;



	/* Initiate connection to host.  On success, connection
	 * is open and this function returns non-null.  On failure,
	 * there is no connection and this function returns null.
	 * This function reports all errors to logfile as appropriate.
	 */

ImapFolder *
openImap(hostname, user, pw, to)
	char	*hostname ;
	char	*user ;
	char	*pw ;
	int	to ;
{
	int	fd ;
	char	buffer[MAX_IMAP_CMD+1] ;
	ImapFolder *im ;

	if( user == NULL ) user = "" ;
	if( pw == NULL ) pw = "" ;

	if( strlen(user) + strlen(pw) > MAX_IMAP_CMD-20 ) {
	  logFile("username and/or password too long\n") ;
	  logFile("  username = %s\n", user) ;
	  return NULL ;
	}

	im = MALLOC(ImapFolder, 1) ;
	if( im == NULL ) {
	  logFile("out of memory in openImap\n") ;
	  return NULL ;
	}

	im->seq = 0 ;
	im->state = NONAUTH ;
	im->nmsg = 0 ;
	im->messages = NULL ;
	im->timeout = 60 ;
	im->capabilities = NULL ;

	signal(SIGPIPE, SIG_IGN) ;

	if( (fd = openNet(hostname, "imap", 143)) < 0 ) {
	  free(im) ;
	  return NULL ;
	}
	im->fd = fd ;

	if( netReadln(fd, buffer, sizeof(buffer), to) <= 0 ) {
	  logFile("unable to connect to imap server %s, %s\n",
	  	hostname, strerror(errno)) ;
	  return ImapAbort(im) ;
	}

	(void) ImapParse(im, NULL, buffer) ;
	if( im->state == LOGOUT )
	  return ImapAbort(im) ;



	if( netWritef(fd, "sm%d CAPABILITY\r\n", im->seq++) < 0 )
	{
	  logFile("unable to connect to imap server %s: %s\n",
	      hostname, strerror(errno)) ;
	  return ImapAbort(im) ;
	}

	if( ImapResponse(im, NULL, buffer, sizeof(buffer)) != OK )
	{
	  logFile("unable to connect to imap server %s, user %s\n",
	    hostname, user) ;
	  return ImapAbort(im) ;
	}



#ifdef	TODO
	if( im->state == NOAUTH && strstr(im->capabilities, "AUTH=KERBEROS_V4"))
	{
	  logFilev(2,"trying Kerberos authentication\n") ;
	  if( AuthKerberos(im, hostname) < 0 )
	    return ImapAbort(im) ;
	};
#endif	/* TODO */


#ifdef	TODO
	if( im->state == NOAUTH && strstr(im->capabilities, "AUTH=X-OTP") )
	{
	  logFilev(2,"trying OTP authentication\n") ;
	  if( AuthOtp(im) < 0 )
	    return ImapAbort(im) ;
	};
#endif	/* TODO */


	/* TODO: kerberos */

	if( im->state == NONAUTH )
	{
	  if( netWritef(fd, "sm%d LOGIN %s %s\r\n", im->seq++, user, pw) < 0 )
	  {
	    logFile("unable to connect to imap server %s: %s\n",
		hostname, strerror(errno)) ;
	    return ImapAbort(im) ;
	  }

	  switch( ImapResponse(im, NULL, buffer, sizeof(buffer)) )
	  {
	    case NO:
	      logFile("Imap authentication failed, host %s, user %s\n",
		hostname, user) ;
	      return ImapAbort(im) ;
	    default:
	      logFile("unable to connect to imap server %s, user %s\n",
		hostname, user) ;
	      return ImapAbort(im) ;
	    case OK:
	      break ;
	  }

	}


	im->timeout = to ;

	return im ;
}


	/* See rfc1731.
	 *	server sends random 32-bit number in base64.
	 *	client sends with Kerberos ticket and authenticator for
	 *		"imap.hostname@realm"
	 *
	 * Returns -1 on fatal error, 0 otherwise.  State may or may not
	 * be authenticated on return.
	 */

static	int
AuthKerberos(im, hostname)
	ImapFolder *im ;
	char	*hostname ;
{
	char	buffer[MAX_IMAP_CMD+1] ;
	unsigned char data[MAX_IMAP_CMD] ;
	int	to = im->timeout ;
	int	fd = im->fd ;
	int	len ;

	if( netWritef(fd,"sm%d AUTHENTICATE KERBEROS_V4\r\n", im->seq++) < 0 ||
	    netReadln(fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("unable to access imap server\n") ;
	  return -1 ;
	}

	if( (len = readBase64(buffer, data, sizeof(data))) < 4 )
	  return 0 ;

	return 0 ;
}



int
imapFolder(im, folder)
	ImapFolder *im ;
	char	*folder ;
{
	char	buffer[MAX_IMAP_CMD+1] ;

	if( strlen(folder) > MAX_IMAP_CMD-10 ) {
	  logFile("folder name too long\n") ;
	  logFile("  %s\n", folder) ;
	  (void) ImapAbort(im) ;
	  return USER_ERR ;
	}

	if( netWritef(im->fd, "sm%d SELECT %s\r\n", im->seq++, folder) < 0 )
	{
	  logFile("imap fatal error setting folder %s: %s\n",
	  	folder, strerror(errno)) ;
	  (void) ImapAbort(im) ;
	  return POP_ERR ;
	}

	switch( ImapResponse(im, NULL, buffer, sizeof(buffer)) )
	{
	  default:
	    logFile("imap open for folder %s failed: %s", folder, buffer) ;
	    (void) ImapAbort(im) ;
	    return POP_ERR ;
	  case OK:
	    break ;
	}

	return EXIT_OK ;
}






#ifdef	COMMENT
/* int
 * imapGetHeader
 *	Read one header from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 */

int
imapGetHeader(im, msg, msgno, to)
	ImapFolder *im ;
	MailMessage *msg ;
	int	msgno ;
	int	to ;
{
	char	buffer[1024] ;

	assert( msgno <= im->nmsg ) ;

	if( msg->state >= HAVE_HEADER )		/* already have it */
	  return EXIT_OK ;

	if( netWritef(im->fd, "TOP %d 0\r\n", msgno) < 0  ||
	    netReadln(im->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("imap fatal error reading header %d: %s\n",
	    	msgno, strerror(errno)) ;
	  (void) ImapAbort(im) ;
	  return INPUT_ERR ;
	}


	if( buffer[0] != '+' )	/* read failed, try reading whole message */
	  return imapGetMessage(im, msg, msgno, to) ;


	return imapGetData(im, msg, msgno, to) ;
}
#endif	/* COMMENT */




/* int
 * imapGetMessage
 *	Read one header from folder, copy to tempfile.  Return 0 on
 *	success, error code on failure.
 */

int
imapGetMessage(msg, newstate)
	MailMessage *msg ;
	MsgState newstate;
{
	ImapFolder *im = (ImapFolder *) msg->popinfo ;
	int	msgno = msg->n ;
	int	to = im->timeout ;
	char	buffer[1024] ;

	assert( msgno <= im->nmsg ) ;

#ifdef	COMMENT
	if( msg->haveBody )		/* already have it */
	  return EXIT_OK ;
#endif	/* COMMENT */

	if( netWritef(im->fd, "RETR %d\r\n", msgno) < 0  ||
	    netReadln(im->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("imap fatal error reading message %d: %s\n",
	    	msgno, strerror(errno)) ;
	  (void) ImapAbort(im) ;
	  return INPUT_ERR ;
	}

	if( buffer[0] != '+' ) {
	  removeCR(buffer) ;
	  logFile("imap fatal error reading message %d: %s",
	  	msgno, buffer) ;
	  (void) ImapAbort(im) ;
	  return INPUT_ERR ;
	}

	return imapGetData(im, msg, msgno, to) ;
}




/* int
 * imapGetData(int fd, MailMessage *msg, int msgno, int to)
 *	Read one message from folder, copy to tempfile.  Return error code on
 *	failure, 0 on success.
 */

static	int
imapGetData(im, msg, msgno, to)
	ImapFolder *im ;
	MailMessage *msg ;
	int	msgno, to ;
{
	char	*ptr ;
	char	buffer[1024] ;
	int	i ;
	int	cancel = False ;
static	char	tmpfilename[MAXPATHLEN] ;

	msg->type = IMAP ;
	msg->n = msgno ;

	/* Open temporary file.  If this fails, get out.  Messages will
	 * remain in remote mailbox.
	 */

	if( msg->file != NULL )
	  fclose(msg->file) ;

#ifdef	COMMENT
	sprintf(tmpfilename, "%s/sortmail%d", tmpdir, getpid()) ;
#endif	/* COMMENT */
	msg->name = tmpfilename ;
	if( (msg->file = tmpOpen(tmpfilename)) == NULL )
	{
	  logFile
	  ("imap fatal error reading message %d, cannot open file %s, %s: %s\n",
	  	msgno, tmpfilename, strerror(errno));
	  (void) ImapAbort(im) ;
	  return TMPFILE_ERR ;
	}
	msg->tmpfile = True ;


	/* Copy input to tmp file.
	 * Scan for key headers lines while at it.
	 */

	msg->inheader = True ;
	msg->scanning = NONE ;

	for(;;)
	{
	  i = netReadln(im->fd, buffer, sizeof(buffer), to) ;
	  if( i <= 0 ) {
	    logFile("imap fatal error reading message %d: %s\n",
	  	msgno, strerror(errno));
	    (void) ImapAbort(im) ;
	    return INPUT_ERR ;
	  }
	  ++msg->lines ;
	  removeCR(buffer) ;

	  if( buffer[0] == '.'  &&  buffer[1] == '\n' )
	    break ;

	  if( *(ptr = buffer) == '.' ) ++ptr ;
	  parseMessage(msg, ptr) ;
	  if( !cancel && fputs(buffer, msg->file) == EOF ) {
	    logFile("imap fatal error reading message %d: %s\n",
	  	msg->name, strerror(errno));
	    (void) ImapAbort(im) ;
	    return TMPFILE_ERR ;
	  }
	  msg->size += strlen(ptr) ;
	}

	parseMessageDone(msg) ;

	return EXIT_OK ;
}


int
imapDeleteMessage(im, msg, to)
	ImapFolder *im ;
	MailMessage *msg ;
	int	to ;
{
	char	buffer[1024] ;

	if( netWritef(im->fd, "DELE %d\r\n", msg->n) < 0  ||
	    netReadln(im->fd, buffer, sizeof(buffer), to) <= 0 )
	{
	  logFile("imap fatal error deleting message %d: %s\n",
	      msg->n, strerror(errno));
	  (void) ImapAbort(im) ;
	  return INPUT_ERR ;
	}

	return EXIT_OK ;
}


static	void
imapFree(im)
	ImapFolder *im ;
{
	close(im->fd) ;
	if( im->messages != NULL ) free(im->messages) ;
	if( im->capabilities != NULL ) free(im->capabilities) ;
	free(im) ;
}

void
closeImap(im)
	ImapFolder *im ;
{
	netWritef(im->fd, "QUIT\r\n") ;
	imapFree(im) ;
}


	/* Read data from server until a Status Response is
	 * received.  Return that status.
	 */

static	parseVal
ImapResponse(im, msg, buffer, len)
	ImapFolder	*im ;
	MailMessage	*msg ;
	char		*buffer ;
	int		len ;
{
	parseVal	rval ;

	do {
	  if( netReadln(im->fd, buffer, len, im->timeout) <= 0 ) {
	    logFile("read error from imap server: %s\n", strerror(errno)) ;
	    (void) ImapAbort(im) ;
	    return DISCONNECT ;
	  }

	  rval = ImapParse(im, msg, buffer) ;
	} while( rval == UNTAGGED ) ;
	return rval ;
}



	/* Process a response from the server.  These may be continuation
	 * requests for when client is making a multi-line request,
	 * untagged responses for when the server has information,
	 * or tagged responses which terminate a client request.
	 *
	 * We never have more than one request active at a time, so there's
	 * no reason to validate tags.
	 */

static	parseVal
ImapParse(im, msg, buffer)
	ImapFolder	*im ;
	MailMessage	*msg ;
	char		*buffer ;
{
	switch(*buffer) {
	  case '+':		/* continuation request */
	    return CONT ;

	  case '*':		/* untagged response */
	    (void) ParseData(im, msg, buffer+1) ;
	    return UNTAGGED ;

	  default:		/* tagged status response */
	    for(; isalnum(*buffer); ++buffer) ;		/* skip tag */
	    buffer = firstNonBlank(buffer) ;
	    if( *buffer == '\0' ) {
	      (void) ImapAbort(im) ;
	      return BAD ;
	    }
	    if( strcmatch(buffer,"OK") ) {
	      (void) ParseCode(im, msg, buffer+2) ;
	      return OK ;
	    }
	    if( strcmatch(buffer,"NO") ) {
	      (void) ParseCode(im, msg, buffer+2) ;
	      return NO ;
	    }
	    if( strcmatch(buffer,"BAD") ) {
	      (void) ParseCode(im, msg, buffer+3) ;
	      return BAD ;
	    }
	    (void) ImapAbort(im) ;
	    return BAD ;
	}
}


	/* parse a server data response (tagged with '*') */

static	parseVal
ParseData(im, msg, buffer)
	ImapFolder	*im ;
	char		*buffer ;
	MailMessage	*msg ;
{
	int	i ;
	long	n = -1 ;
	char	*ptr = buffer ;

	ptr = firstNonBlank(ptr) ;		/* skip to message */

	if( isdigit(*ptr) )
	{
	  n = strtol(ptr, &ptr, 10) ;
	  ptr = firstNonBlank(ptr) ;		/* skip to message */
	}

	if( strcmatch(ptr, "OK") ) {
	  (void) ParseCode(im, msg, ptr+2) ;
	  return OK ;
	}

	if( strcmatch(ptr, "NO") ) {
	  (void) ParseCode(im, msg, ptr+2) ;
	  return NO ;
	}

	else if( strcmatch(ptr, "BYE") )
	  im->state = LOGOUT ;

	else if( strcmatch(ptr, "PREAUTH") )
	  im->state = AUTH ;

	else if( strcmatch(ptr, "EXISTS") ) {
	  im->messages = (ImapMsg *)realloc(im->messages, n*sizeof(ImapMsg)) ;
	  if( im->messages == NULL && n > 0 ) {
	    logFile("out of memory in Imap ParseData") ;
	    (void)ImapAbort(im) ;
	    return DISCONNECT ;
	  }
	  im->nmsg = n ;
	}

	else if( strcmatch(ptr, "EXPUNGE") ) {
	  if( n > 0 ) {
	    for(i=n; i < im->nmsg; ++i)
	      im->messages[i-1] = im->messages[i] ;
	    memset(&(im->messages[im->nmsg - 1]), 0, sizeof(ImapMsg)) ;
	    --im->nmsg ;
	  }
	}
	else if( strcmatch(ptr, "FETCH") )
	  return ImapFetch(im, msg, buffer) ;

	else if( strcmatch(ptr, "FLAGS") )
	{
	  /* TODO */
	}

	else if( strcmatch(ptr, "CAPABILITY") )
	{
	  if( im->capabilities != NULL ) free(im->capabilities) ;
	  im->capabilities = strdup(ptr) ;
	}

	/* ignore LIST, LSUB, STATUS, SEARCH, RECENT */



	return OK ;
}


	/* parse ancillary response code in server data */

static	parseVal
ParseCode(im, msg, buffer)
	ImapFolder	*im ;
	char		*buffer ;
	MailMessage	*msg ;
{
	char	*ptr ;

	buffer = strchr(buffer,'[') ;
	if( buffer != NULL )
	{
	  ++buffer ;
	  ptr = strchr(buffer,']') ;
	  removeNL(ptr) ;
	  if( strcmatch(buffer, "ALERT") ) {
	    if( ptr == NULL ) ptr = buffer+5 ;
	    fprintf(stderr, "%s\n", ptr+1) ;
	  }
	  else if( strcmatch(buffer, "NEWNAME") ) {
	    if( ptr == NULL ) ptr = buffer+7 ;
	    logFile( "Mailbox renamed: %s\n", ptr+1) ;
	  }
	  else if( strcmatch(buffer, "PARSE") ) {
	    if( ptr == NULL ) ptr = buffer+5 ;
	    logFile( "Mailbox parse error: %s\n", ptr+1) ;
	  }
	  else if( strcmatch(buffer, "PERMANENTFLAGS") ) {
	    if( ptr == NULL ) ptr = buffer+14 ;
	    logFile( "Attempt to change unsupported flag: %s\n", ptr+1) ;
	  }
	  else if( strcmatch(buffer, "TRYCREATE") ) {
	    if( ptr == NULL ) ptr = buffer+9 ;
	    logFile( "append/copy failed: %s\n", ptr+1) ;
	  }
	  /* READ-ONLY, READ-WRITE, UIDVALIDITY, UNSEEN, etc. are
	   * information only; ignore them.
	   */
	}
	return OK ;
}


	/* parse FETCH response */

static	parseVal
ImapFetch(im, msg, buffer)
	ImapFolder	*im ;
	char		*buffer ;
	MailMessage	*msg ;
{
	return OK ;		/* TODO */
}




static	ImapFolder *
ImapAbort(im)
	ImapFolder *im ;
{
	imapFree(im) ;
	return NULL ;
}
