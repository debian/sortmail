/*	@(#)netutils.h 1.2 02/03/11 falk	*/
/*	$Id: netutils.h,v 1.1.1.1 2002/03/12 05:38:53 efalk Exp $	*/

#ifdef	__STDC__
extern	int	openNet(char *hostname, char *servicename, int dfltport ) ;
extern	int	netWrite( int fd, char *buf, size_t count ) ;
extern	int	netWritef(int fd, char *fmt, ...) ;
extern	int	netRead( int fd, char *buf, size_t nbyte, int to ) ;
extern	int	netReadln( int fd, char *buf, size_t nbyte, int to ) ;
extern	int	netCommand( int fd, char *cmd, char *buf, size_t nbyte, int to ) ;
extern	void	removeCR(char *) ;
#else
extern	int	openNet() ;
extern	int	netWrite() ;
extern	int	netWritef() ;
extern	int	netRead() ;
extern	int	netReadln() ;
extern	int	netCommand() ;
extern	void	removeCR() ;
#endif
