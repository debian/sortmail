/*	@(#)pop3.h 1.2 02/03/11 falk	*/
/*	$Id: pop3.h,v 1.3 2003/08/21 15:59:26 efalk Exp $	*/


	/* info known about a pop3 folder. */

typedef	struct {
	  int	fd ;
	  int	nmsg ;
	  int	timeout ;
	  int	lines ;		/* # lines read so far in most recent msg */
	  long	size ;		/* # bytes read so far in most recent msg */
	} Pop3Folder ;


#define	POP3_OK		0
#define	POP3_ERR	1	/* Protocol error */
#define	POP3_TIMEOUT	2	/* Network timeout */
#define	POP3_TMPFILE	3	/* Error writing output file */



extern	Pop3Folder *Pop3Open( char *hostname, char *user, char *pw,
			int to, int noApop) ;
extern	int	Pop3ReadMessage( Pop3Folder *, FILE *, int msgno, int timeout);
extern	int	Pop3ReadHeader( Pop3Folder *, FILE *, int msgno, int timeout);
extern	int	Pop3DeleteMessage( Pop3Folder *p3, int msgno, int timeout) ;
extern	void	Pop3Close( Pop3Folder *p3) ;
