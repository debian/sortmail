#ifndef lint
static const char sccsid[] = "@(#)netutils.c 1.4 02/03/11 falk" ;
static const char rcsid[] = "$Id: netutils.c,v 1.4 2003/12/22 06:03:14 efalk Exp $" ;
#endif

/*	NETUTILS.C	-- manage POP and IMAP protocols.
 *
 *
 * int
 * openNet(char *hostname, char *service, int dflt)
 *	Open a network connection, return fd.  Return -1 on failure.
 *
 * int
 * netWrite(int fd, char *buf, size_t count)
 *
 * int
 * netWritef(int fd, char *fmt, ...)
 *
 * int
 * netReadln(int fd, char *buf, size_t nbyte, int to)
 *	Like read(2), but times out after 'to' seconds
 *
 * void
 * removeCR(char *buffer)
 */


#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#ifdef	__STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "sortmail.h"
#include "utils.h"
#include "netutils.h"


/*---------------------------------------------------------------------------
 * Open a network connection given a hostname, a service name, and a default
 * port in case the service is not found by name.
 *
 * TODO:  Come up with some way for this to time out.  This is highly
 * difficult, because the gethostbyname() and connect() system calls
 * have no time out and can theoretically hang forever.  This would
 * have to be implemented by forking a child process or thread to
 * make the connection.
 */

int
openNet( char *hostname, char *servicename, int port )
{
	struct servent *servent ;
	struct hostent *hostent ;
	struct protoent *protoent ;
	struct sockaddr_in addr ;
	char	**addrP ;
	int	fd ;
	char	*protoname = "tcp" ;
	int	proto = IPPROTO_TCP ;

	if( (hostent = gethostbyname(hostname)) == NULL )
	{
	  logFile("cannot connect to %s, %s\n",
	    hostname, hstrerror(h_errno)) ;
	  return -1 ;
	}

	if( (servent = getservbyname(servicename,"tcp")) != NULL )
	{
	  port = servent->s_port ;
	  protoname = servent->s_proto ;
	}
	else
	  port = htons(port);

	if( (protoent = getprotobyname(protoname)) != NULL )
	  proto = protoent->p_proto ;

	if( (fd = socket(AF_INET, SOCK_STREAM, proto)) == -1 )
	{
	  logFile("cannot connect to %s, %s\n", hostname, strerror(errno));
	  return -1 ;
	}

	memset(&addr, 0, sizeof(addr)) ;
	addr.sin_family = AF_INET ;
	addr.sin_port = port ;

	logFilev(2, "connect to host %s\n", hostname) ;

	/* Try addresses until we find one that works. */

	addrP = hostent->h_addr_list ;
	do {
	  addr.sin_addr = *(struct in_addr *)*addrP++ ;

	  if( connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == 0 )
	    return fd ;
	}
	while( *addrP != NULL ) ;

	logFile("cannot connect to %s, %s\n", hostname, strerror(errno)) ;
	close(fd) ;

	return -1 ;
}


	/* like write(2), but writes entire buffer */

int
netWrite(int fd, char *buf, size_t count)
{
	size_t	n, len=count ;

	logFilev(3, "transmit %s", buf) ;

	while( len > 0 ) {
	  n = write(fd, buf, len) ;
	  if( n <= 0 )
	    return n ;
	  buf += n ;
	  len -= n ;
	}

	return count ;
}


#ifdef	__STDC__
int
netWritef(int fd, char *fmt, ...)
#else
int
netWritef(va_alist)
	va_dcl
#endif
{
	va_list	args ;
	char	buffer[1024] ;
	int	rval ;

#ifndef	__STDC__
	int	fd ;
	char	*fmt ;
	va_start(args) ;
	fd = va_arg(args, int) ;
	fmt = va_arg(args, char*) ;
#else
	va_start(args, fmt) ;
#endif

	vsprintf(buffer, fmt, args) ;
	rval = netWrite(fd, buffer, strlen(buffer)) ;
	va_end(args) ;
	return rval ;
}


	/* Like read(2), but can time out.  */

int
netRead(int fd, char *buf, size_t nbyte, int to)
{
	fd_set	readfds ;
	struct timeval timeout ;
	int	i ;

	timeout.tv_sec = to ;
	timeout.tv_usec = 0 ;
	FD_ZERO(&readfds) ;
	FD_SET(fd, &readfds) ;

	for(;;)
	{
	  logFilev(4, "About to wait for input\n") ;
	  i = select(fd+1, &readfds, NULL,NULL, &timeout) ;
	  if( i > 0 )
	  {
	    do {
	      i = read(fd,buf,nbyte) ;
	    } while( i <= 0 && errno == EINTR ) ;
	    return i ;
	  }
	  else if( i < 0 )
	  {
	    if( errno != EINTR ) {
	      logFile("netRead: i=%d, errno=%d\n", i,errno) ;
	      return i ;
	    }
	  }
	  else {
	    errno = ETIMEDOUT ;
	    return -1 ;
	  }
	}
}


	/* Like netRead().  Reads one line or the
	 * maximum specified by the caller.  Buffer is nul-terminated
	 *
	 * I'm too lazy to buffer the data, so I use peek to scan ahead
	 * for newlines.
	 */

int
netReadln(int fd, char *buf, size_t nbyte, int to)
{
	fd_set	readfds ;
	struct timeval timeout ;
	int	i ;
	char	*ibuf = buf, *ptr ;
	size_t	rlen = 0 ;

	timeout.tv_sec = to ;
	timeout.tv_usec = 0 ;
	FD_ZERO(&readfds) ;
	FD_SET(fd, &readfds) ;

	/* Reserve room for trailing nul */
	--nbyte;

	while( nbyte > 0 )
	{
	  logFilev(4, "About to wait for input\n") ;
	  i = select(fd+1, &readfds, NULL,NULL, &timeout) ;
	  if( i > 0 )
	  {
	    do {
	      i = recv(fd, ibuf,nbyte, MSG_PEEK) ;
	    } while( i < 0 && errno == EINTR ) ;
	    if( i<=0 )
	      return i ;

	    if( (ptr = memchr(ibuf, '\n', i)) != NULL )
	      nbyte = ptr - ibuf + 1 ;

	    do {
	      i = read(fd,ibuf,nbyte) ;
	    } while( i < 0 && errno == EINTR ) ;
	    if( i<=0 )
	      return i ;

	    if( i > 0 ) {
	      nbyte -= i ;
	      ibuf += i ;
	      rlen += i ;
	    }
	  }

	  else if( i < 0 )
	  {
	    if( errno != EINTR ) {
	      logFile("netReadln: i=%d, errno=%d\n", i,errno) ;
	      return i ;
	    }
	  }
	  else {
	    errno = ETIMEDOUT ;
	    return -1 ;
	  }
	}

	*ibuf = '\0' ;

	logFilev(4, "receive %s", buf) ;

	return rlen ;
}




void
removeCR(char *buffer)
{
	char	*ptr ;

	if( (ptr = strchr(buffer,'\r')) != NULL ) {
	  *ptr = '\n' ;
	  *++ptr = '\0' ;
	}
}



#ifdef	COMMENT

	/* send a command, return response */
int
netCommand(int fd, char *cmd, char *line, int nbyte, int to)
{
	int	rval ;

	if( (rval=netWrite(fd, cmd)) < 0 )
	  return rval ;

	return getRmtResponse(fd, line, to) ;
}
#endif	/* COMMENT */
